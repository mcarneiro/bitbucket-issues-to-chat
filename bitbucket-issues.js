
module.exports = (function(){

	'use strict';

	var fs = require('fs'),
		url = require('url'),
		util = require('util'),
		mustache = require('mustache'),
		path = require('path'),
		mout = require('mout/object'),
		OAuth = require('oauth').OAuth,

		timer = null,
		status = {},
		lastActivity = new Date(),
		connectionStatus = 'iddle',
		config = mout.merge({
			intervalTime: 300,
			bitbucket: {
				repo: null,
				consumerKey: null,
				consumerKeySecret: null,
				statusFilter: 'new|open|resolved',
				issuesURL: 'https://bitbucket.org/%s/issue/',
				issuesAPI: 'https://bitbucket.org/api/1.0/repositories/%s/issues/',
				commentsAPI: 'https://bitbucket.org/api%s/comments'
			},
			messages: {
				newIssue: 'Issue #{{issue.local_id}} ({{issue.url}}) - {{issue.title}} criada por {{issue.reported_by.display_name}} - ({{issue.metadata.kind}}, {{issue.priority}}): "{{issue.content}}"',
				commentedIssue: '{{comment.author_info.display_name}} comentou na Issue #{{issue.local_id}} ({{issue.url}}) - {{issue.title}}: "{{comment.content}}"',
				resolvedIssue: 'Issue #{{issue.local_id}} ({{issue.url}}) {{issue.title}} resolvida',
				generalActivity: 'Atividade na issue #{{issue.local_id}} ({{issue.url}}) {{issue.title}}'
			}
		}, loadConfig()),
		oauth = new OAuth(
			null,
			null,
			config.bitbucket.consumerKey,
			config.bitbucket.consumerKeySecret,
			'1.0',
			null,
			'HMAC-SHA1'
		),
		service = loadService(config.service);

	function moduleExists(name) {
		try {
			return !!require.resolve(name);
		} catch(e) {}
		return false;
	}
	function loadService(name) {
		switch (true) {
			case (moduleExists('./services/bbi-service-' + name)):
				return require('./services/bbi-service-' + name);
			case (moduleExists('bbi-service-' + name)):
				return require('bbi-service-' + name);
			case (moduleExists(name)):
				return require(name);
		}
		throw new Error(util.format('There\'s no service called "%s". Is there a node module as "bbi-service-%s" or "%s"?', name, name, name));
	}

	function loadConfig() {
		if (fs.existsSync('config.json')) {
			return JSON.parse(fs.readFileSync('config.json') || '{}')
		} else {
			fs.createWriteStream('config.json', {mode: '0666'});
			fs.writeFileSync('config.json', fs.readFileSync(path.resolve(__dirname, 'config.json.template')));
			throw new Error(util.format('File "config.json" not found and was created on %s. Fill it up with the app values.', path.resolve('.') + 'config.json'));
		}
	}
	function verifyConfig() {
		var errorCount = 0;

		function checkProperty(value, message) {
			if (!value) {
				console.error('> ERRO: ', message.split('\n').join('\n         '));
				errorCount++;
			}
		}

		var messages = {messages: config.messages};
		delete config.messages;
		config = mout.merge(messages, service.getDefaultConfig(), config);

		service.setConfig(config);
		service.verifyConfig(checkProperty);

		config.bitbucket.issuesURL = util.format(config.bitbucket.issuesURL, config.bitbucket.repo);
		config.bitbucket.issuesAPI = util.format(config.bitbucket.issuesAPI, config.bitbucket.repo);

		checkProperty(config.bitbucket.repo, 'Repositório do bitbucket precisa ser definido no format <USER>/<REPO>.');
		checkProperty(config.bitbucket.consumerKey, 'Consumer key de um usuário com acesso de leitura precisa ser definido.\nVá em https://bitbucket.org/account/user/<USER_NAME>/api e "add consumer"');
		checkProperty(config.bitbucket.consumerKeySecret, 'Consumer keu Secret de um usuário com acesso de leitura precisa ser definido.\nVá em https://bitbucket.org/account/user/<USER_NAME>/api e "add consumer"');

		if (errorCount) {
			throw new Error("Algumas propriedades de configuração não estão definidas. Está faltando o arquivo config.json?");
		}
	}

	function loadStatus() {
		if (!fs.existsSync('status.json')) {
			fs.createWriteStream('status.json', {mode: '0666'});
			fs.writeFileSync('status.json', JSON.stringify({
				lastActivity: String(new Date())
			}));
		}
		status = JSON.parse(fs.readFileSync('status.json'));
		lastActivity = new Date(status.lastActivity);
	}
	function getBitbucketIssues() {
		console.log('3. Getting Bitbucket issues for ' + config.bitbucket.repo);
		oauth.get(config.bitbucket.issuesAPI, null, null, function(error, data, result){
			if (error) {
				if (error.statusCode == 401) {
					console.log('3.1 an permission error has occurred. Check your credentials.');
				} else {
					console.log('3.1 an error error has occurred.');
					next();
				}
				return;
			}
			getOnlyNew(JSON.parse(data));
		});
	}
	function getOnlyNew(data) {
		console.log('4. Filtering only newly updated issues');
		var count = 0;
		data.issues.forEach(function(issue, i){
			var itemDate = new Date(issue.utc_last_updated);
			if (Number(itemDate) > Number(lastActivity)) {
				requestCommentsForIssue(issue);
				count++;
			}
		});
		if (count === 0) {
			console.log('4.1 Nothing found, so there\'s nothing to do here');
			next();
		}
	}
	function requestCommentsForIssue(issue) {
		console.log('5. Getting comments for changed issues');
		oauth.get(util.format(config.bitbucket.commentsAPI, issue.resource_uri), null, null, function(error, data, result){
			if (error) {
				if (error.statusCode == 401) {
					console.log('3.1 an permission error has occurred. Check your credentials.');
				} else {
					console.log('3.1 an error error has occurred.');
					next();
				}
				return;
			}
			getOnlyNewComments(issue, JSON.parse(data));
		});
	}
	function getOnlyNewComments(issue, comments) {
		console.log('5.1 Filtering only new comments');
		var newComments = [];
		comments.forEach(function(item, i){
			var itemDate = new Date(item.utc_updated_on);
			if (Number(itemDate) > Number(lastActivity)) {
				newComments.push(item);
			}
		});
		sendMessage(issue, newComments);
	}
	function formatMessage(issue, comments) {

		var data = {issue: issue, comments: comments };

		data.issue.url = config.bitbucket.issuesURL + issue.local_id;
		data.issue.content = issue.content.replace(/(\n|\*|_|`)/g, '').slice(0, 50) + '...';

		if (!issue.status.match(new RegExp(config.bitbucket.statusFilter, 'gim'))) {
			return '';
		}

		switch (true) {
			// Nova issue
			case (issue.utc_last_updated === issue.utc_created_on):
				return mustache.render(config.messages.newIssue, data);
			// Atividade resolvida
			case (issue.status === 'resolved'):
				return mustache.render(config.messages.resolvedIssue, data);
			// Novo comentário
			case (comments.length > 0):
				var message = [];
				data.comments.forEach(function(comment){
					data.comment = comment;
					data.comment.content = data.comment.content.replace(/\n/g, ' ');
					message[message.length] = mustache.render(config.messages.commentedIssue, data);
				});
				return message.join('\n\n');
			// Nova atividade
			default:
				return mustache.render(config.messages.generalActivity, data);
		}
		return '';
	}
	function sendMessage(issue, comments) {
		var message = formatMessage(issue, comments);
		console.log('6. Sending message: ', message);

		service.send(message, updateVerificationStatus, next);
	}
	function updateVerificationStatus() {
		console.log('7. updating verification status and last date.');
		status.lastActivity = String(new Date());
		fs.writeFileSync('status.json', JSON.stringify(status));
		next();
	}

	function next() {
		console.log('Next verification in ' + (config.intervalTime / 60) + ' minute(s).');
		connectionStatus = 'iddle';
		clearTimeout(timer);
		timer = setTimeout(function(){
			if (connectionStatus === 'iddle') {
				connectionStatus = 'working';
				try {
					runVerification();
				} catch(e) {
					console.info('error:', e);
					next();
				}
			}
		}, config.intervalTime * 1000);
	}

	function runVerification() {
		loadStatus();
		getBitbucketIssues();
	}

	verifyConfig();
	runVerification();

}());