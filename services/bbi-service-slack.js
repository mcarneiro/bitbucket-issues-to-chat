
module.exports = (function(){

	'use strict';

	var util = require('util'),
		url = require('url'),
		mout = require('mout').object,
		https = require('https'),
		config = {};

	return {
		getDefaultConfig: function() {
			return {
				slack: {
					token: null,
					team: null,
					postURL: 'https://%s.slack.com/services/hooks/incoming-webhook?token=%s',
					data: {
						'channel': '#slack_sandbox',
						'mrkdwn': true,
						'parse': 'none',
						'username': 'Bitbucket Issues',
						'icon_url': 'https://slack.global.ssl.fastly.net/20653/img/services/bitbucket_128.png'
					}
				},
				messages: {
					newIssue: '*<{{{issue.url}}}|Issue #{{{issue.local_id}}}> _{{{issue.title}}}_* criada por {{{issue.reported_by.display_name}}} — (`{{{issue.metadata.kind}}}` — `{{{issue.priority}}}`): "{{{issue.content}}}"',
					commentedIssue: '{{{comment.author_info.display_name}}} comentou na *<{{{issue.url}}}|Issue #{{{issue.local_id}}}> _{{{issue.title}}}_*: "{{{comment.content}}}"',
					resolvedIssue: '*<{{{issue.url}}}|Issue #{{{issue.local_id}}}> _{{{issue.title}}}_* resolvida',
					generalActivity: 'Atividade na *<{{{issue.url}}}|issue #{{{issue.local_id}}}> _{{{issue.title}}}_*'
				}
			};
		},
		verifyConfig: function(checkProperty) {
			checkProperty(config.slack.token, 'Token de integração do Slack (Incoming Webhook) precisa ser definido.\nAdicione um novo em https://<TEAM_NAME>.slack.com/services/new');
			checkProperty(config.slack.team, 'Time do slack precisa ser definido.');
			config.slack.postURL = util.format(config.slack.postURL, config.slack.team, config.slack.token);
		},
		send: function(message, callbackSuccess, callBackError) {
			console.info(config.slack.postURL);
			var parsedURL = url.parse(config.slack.postURL),
				req,
				data = mout.merge(config.slack.data, {
					'text': message
				}),
				options = {
					host: parsedURL.host,
					port: parsedURL.port,
					path: parsedURL.path,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json; charset=utf-8'
					}
				};
			req = https.request(options, function(res) {
				if (res.statusCode == 200) {
					callbackSuccess();
				} else {
					callBackError();
				}
			});
			req.on('error', function(e) {
				callBackError();
			});
			req.end(JSON.stringify(data));
		},
		setConfig: function(data) {
			config = data;
		}
	};
}());