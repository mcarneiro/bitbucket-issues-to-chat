
module.exports = (function(){

	'use strict';

	var util = require('util'),
		mout = require('mout').object,
		config = {};

	return {
		getDefaultConfig: function() {
			return {
				serviceName: {
					token: null,
					postURL: 'https://service.com/api/1/services/generic/%s',
					data: {
						// usually used to send data to the server
					}
				},
				messages: {
					// this messages can receive wildcards to replace with the values of the issue / comment object.
					newIssue: 'Issue #{{issue.local_id}} ({{issue.url}}) - {{issue.title}} criada por {{issue.reported_by.display_name}} - ({{issue.metadata.kind}}, {{issue.priority}}): "{{issue.content}}"',
					commentedIssue: '{{comment.author_info.display_name}} comentou na Issue #{{issue.local_id}} ({{issue.url}}) - {{issue.title}}: "{{comment.content}}"',
					resolvedIssue: 'Issue #{{issue.local_id}} ({{issue.url}}) {{issue.title}} resolvida',
					generalActivity: 'Atividade na issue #{{issue.local_id}} ({{issue.url}}) {{issue.title}}'
				}
			};
		},
		verifyConfig: function(checkProperty) {
			// this will run before everything, Here, "config" will be defined and will have all merged data from config.json
			//   checkProperty(value, msg) will record the undefined required variables and will throw an error.
			// Example:
			//   checkProperty(config.serviceName.token, 'Token de integração do serviço (Custom) precisa ser definido.\nAdquira o valor em https://servico.com/docs/integrations/generic/');
			//   config.serviceName.postURL = util.format(config.hall.postURL, config.hall.token);
		},
		send: function(message, callbackSuccess, callbackError) {
			// Code to send to the chat service.
			//   "message" is the formatted message
			//   "callbackSuccess" is a function that'll record the current time on status.json
			//   "callbackError" is a function that'll just wait for the next time to check bitbucket REST API
		},
		setConfig: function(data) {
			// This will run after config is merged and ready to use.
			config = data;
		}
	};
}());