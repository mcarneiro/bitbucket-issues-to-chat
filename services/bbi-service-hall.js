
module.exports = (function(){

	'use strict';

	var util = require('util'),
		url = require('url'),
		jju = require('jju'),
		mout = require('mout').object,
		https = require('https'),
		config = {};

	return {
		getDefaultConfig: function() {
			return {
				hall: {
					token: null,
					postURL: 'https://hall.com/api/1/services/generic/%s',
					data: {
						'title': 'Bitbucket Issues',
						'picture': 'https://d2cqobfvgy9i6o.cloudfront.net/assets/services/bitbucket-eb9391ceb9c8c0545a27169c13f3971b.png'
					}
				},
				messages: {
					newIssue: '<b><a href="{{{issue.url}}}">Issue #{{{issue.local_id}}}</a> <i>{{{issue.title}}}</i></b> criada por {{{issue.reported_by.display_name}}} — (<code>{{{issue.metadata.kind}}}</code> — <code>{{{issue.priority}}}</code>): "{{{issue.content}}}"',
					commentedIssue: '{{{comment.author_info.display_name}}} comentou na <b><a href="{{{issue.url}}}">Issue #{{{issue.local_id}}}</a> <i>{{{issue.title}}}</i></b>: "{{{comment.content}}}"',
					resolvedIssue: '<b><a href="{{{issue.url}}}">Issue #{{{issue.local_id}}}</a> <i>{{{issue.title}}}</i></b> resolvida',
					generalActivity: 'Atividade na <b><a href="{{{issue.url}}}">issue #{{{issue.local_id}}}</a> <i>{{{issue.title}}}</i></b>'
				}
			};
		},
		verifyConfig: function(checkProperty) {
			checkProperty(config.hall.token, 'Token de integração do Hall (Custom) precisa ser definido.\nAdquira o valor em https://hall.com/docs/integrations/generic/');
			config.hall.postURL = util.format(config.hall.postURL, config.hall.token);
		},
		send: function(message, callbackSuccess, callBackError) {
			var parsedURL = url.parse(config.hall.postURL),
				data = jju.stringify(mout.merge(config.hall.data, {
					message: message
				}), { mode: 'json', ascii: true }),
				req,
				options = {
					host: parsedURL.host,
					port: parsedURL.port,
					path: parsedURL.path,
					method: 'POST',
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Content-Length': data.length
					}
				};
			req = https.request(options, function(res) {
				console.info(res.statusCode);
				if (res.statusCode == 201) {
					callbackSuccess();
				} else {
					callBackError();
				}
			});
			req.on('error', function(e) {
				callBackError();
			});
			req.end(data);
		},
		setConfig: function(data) {
			config = data;
		}
	};
}());